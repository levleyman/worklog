package home.leyman;

import com.google.api.services.calendar.model.Event;

import java.util.HashMap;
import java.util.Map;

public class LogCollection
{
    private final Map<LogId, Log> logs = new HashMap<>();

    public void add(Event event)
    {
        LogId id = new LogId(event);

        if (!logs.containsKey(id)) {
            logs.put(id, new Log());
        }

        logs.get(id).append(event);
    }

    public Map<LogId, Log> all()
    {
        return logs;
    }
}
