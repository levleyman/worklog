package home.leyman;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.CalendarList;
import com.google.api.services.calendar.model.CalendarListEntry;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class Worklog
{
    private static final String DIVIDER = "-".repeat(80);

    private static final String APPLICATION_NAME = "Worklog";

    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    private static final String TOKENS_DIRECTORY_PATH = "tokens";

    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */
    private static final List<String> SCOPES = Collections.singletonList(CalendarScopes.CALENDAR_READONLY);

    private static final String CREDENTIALS_FILE_PATH = "/credentials.json";

    private final Calendar calendar;

    public Worklog() throws IOException, GeneralSecurityException
    {
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        calendar = new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();
    }

    /**
     * Creates an authorized Credential object.
     *
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */
    private Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException
    {
        // Load client secrets.
        InputStream in = Worklog.class.getResourceAsStream(CREDENTIALS_FILE_PATH);

        if (in == null) {
            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
        }

        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();

        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }

    public CalendarListEntry findCalendar(String name) throws IOException
    {
        String page = null;
        CalendarListEntry result = null;

        do {
            CalendarList calendarList = calendar.calendarList().list().setPageToken(page).execute();
            Optional<CalendarListEntry> calendarDocDoc = calendarList
                    .getItems()
                    .stream()
                    .filter(calendarListEntry -> calendarListEntry.getSummary().equals(name))
                    .findFirst();

            if (calendarDocDoc.isPresent()) {
                result = calendarDocDoc.get();
                page = null;
            } else {
                page = calendarList.getNextPageToken();
            }

        }
        while (page != null);

        return result;
    }

    public Events listCalendarEventsFromTimeToNow(String calendarName, LocalDateTime minTime, String pageToken) throws IOException
    {
        var cal = findCalendar(calendarName);

        if (cal == null) {
            return new Events();
        }

        var list = calendar
                .events()
                .list(cal.getId())
                .setMaxResults(20)
                .setTimeMin(DateTime.parseRfc3339(minTime.toString()))
                .setTimeMax(DateTime.parseRfc3339(LocalDateTime.now().toString()))
                .setOrderBy("startTime")
                .setSingleEvents(true);

        if (pageToken != null) {
            list.setPageToken(pageToken);
        }

        return list.execute();
    }

    public static void main(String[] args) throws GeneralSecurityException, IOException
    {

        if (args.length < 2) {
            System.err.println("Required 2 arguments.\n1 - days offset from now\n2 - used calendar name");
            return;
        }

        var app = new Worklog();

        long fromDaysAgo = Long.parseLong(args[0]);
        String targetCalendarName = args[1];
        System.out.println("Use calendar '" + targetCalendarName + "'");

        LocalDateTime minTime = LocalDateTime.now().minusDays(fromDaysAgo);
        System.out.println("From " + minTime.format(DateTimeFormatter.ISO_LOCAL_DATE) + " to NOW\n\n" + DIVIDER);

        var logCollections = new LogCollection();
        String pageToken = null;

        do {
            var events = app.listCalendarEventsFromTimeToNow(targetCalendarName, minTime, pageToken);

            if (events.getItems().isEmpty()) {
                System.out.println("No events found.");
                pageToken = null;
            } else {

                for (Event event : events.getItems()) {
                    logCollections.add(event);
                }

                pageToken = events.getNextPageToken();
            }
        }
        while (pageToken != null);

        // TODO: sort events by date

        for (var entry : logCollections.all().entrySet()) {
            String hours = entry.getValue().getDuration().toHoursPart() > 0
                    ? entry.getValue().getDuration().toHoursPart() + "h"
                    : "";
            String minutes = entry.getValue().getDuration().toMinutesPart() > 0
                    ? entry.getValue().getDuration().toMinutesPart() + "m"
                    : "";
            System.out.println(entry.getKey().getName()
                    + " ("+entry.getKey().getDateTime().format(DateTimeFormatter.ISO_LOCAL_DATE)+") "
                    + hours + (minutes.isEmpty() ? minutes : " " + minutes) + "\n"
                    + entry.getValue().getDescription() + "\n" + DIVIDER);
        }
    }
}
