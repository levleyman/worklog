package home.leyman;

import com.google.api.services.calendar.model.Event;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Log
{
    private String description = "";

    private Duration duration = Duration.ZERO;

    public void append(Event event)
    {
        description += event.getDescription() != null
                ? (description.isEmpty() ? "" : "\n") + event.getDescription()
                : "";
        duration = duration.plus(Duration.between(
                LocalDateTime.parse(event.getStart().getDateTime().toStringRfc3339(),
                        DateTimeFormatter.ISO_OFFSET_DATE_TIME),
                LocalDateTime.parse(event.getEnd().getDateTime().toStringRfc3339(),
                        DateTimeFormatter.ISO_OFFSET_DATE_TIME)));
    }

    public String getDescription()
    {
        return description;
    }

    public Duration getDuration()
    {
        return duration;
    }
}
