package home.leyman;

import com.google.api.services.calendar.model.Event;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LogId
{
    private final String name;

    private final LocalDateTime dateTime;

    public LogId(Event event)
    {
        name = event.getSummary().trim();
        var start = event.getStart().getDateTime();

        if (start == null) {
            start = event.getStart().getDate();
        }

        dateTime = LocalDateTime.parse(start.toStringRfc3339(), DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    public String getName()
    {
        return name;
    }

    public LocalDateTime getDateTime()
    {
        return dateTime;
    }
}
